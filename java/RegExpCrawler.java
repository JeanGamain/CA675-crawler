import java.io.IOException;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.Writer;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.HttpURLConnection;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.CRC32;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.nio.file.*;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.ChecksumFileSystem;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class RegExpCrawler {
  public static enum counter {
    URL_COUNT,
    TOTAL_URL_COUNT,
    FOUND_URL_COUNT,
    ERROR_COUNT,
    WARNING_COUNT,
    DATA_COUNT,
  }
  public static class TokenizerMapper extends Mapper<Object, Text, Text, Text>{ 
    public void map(Object k, Text val, Context context) throws IOException, InterruptedException {
	String csv = val.toString();
	if (csv.lastIndexOf(';') == -1) {
	    System.out.println("wat:" + csv);
	}
	context.write(new Text(csv.substring(0, csv.indexOf(';'))), new Text(csv.substring(csv.indexOf(';') + 1)));
    }
  }

  public static class DistinctDataReducer extends Reducer<Text, Text, Text, Text> {
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
      context.getCounter(RegExpCrawler.counter.DATA_COUNT).increment(1);
      context.write(key, values.iterator().next());
    }
  }

  public static class HttpReducer extends Reducer<Text, Text, Text, Text> {
    private static final Text zero = new Text("0");
    private MultipleOutputs<Text, Text> output;
    
    @Override
    public void setup(Context context) {
	output = new MultipleOutputs<Text, Text>(context);
    }

    @Override
    public void cleanup(Context context) throws IOException, InterruptedException
    {
      output.close();
    }

    private Boolean testCannonicUrl(String html, String url) {
      Matcher cannonicUrlMatcher = Pattern.compile("<link[ ]+rel[ ]*=[ ]*('|\")canonical('|\")[ ]+href[ ]*=[ ]*('|\")([^\"\']+)('|\")\\/>", Pattern.CASE_INSENSITIVE).matcher(html);
      if (cannonicUrlMatcher.find()) { // canonical url found
	String cannonicUrl = parseUrl(url.substring(0, url.substring(9).indexOf("/") + 9), url, cannonicUrlMatcher.group(5));
	if (url.indexOf(cannonicUrl) != -1) {
	  return true;
	}
      }
      return false;
    }

    private String parseUrl(String connectInfo, String url, String newurl) {
      Pattern completeUrlMatch = Pattern.compile("(https?|ftp)://", Pattern.CASE_INSENSITIVE);

      if (newurl.indexOf("//") == 0) {
        newurl = "http:" + newurl;
      }
      try {
        newurl = URLDecoder.decode(newurl, "ISO-8859-1");
      } catch (Exception e) {
      }
      newurl = StringEscapeUtils.unescapeHtml4(newurl); // decode %20 and &amp stuff ...
      if (completeUrlMatch.matcher(newurl).find()) { // its a complete url
	return newurl;
      } else { // its a local site path
        if (newurl.indexOf("/") == 0) { // absolute path
	  newurl = connectInfo + newurl;		    
	} else { // relative path
	  String base = url.substring(0, url.lastIndexOf("/"));
	  int a, b;
	  while ((a = newurl.indexOf("../")) == 0 || (b = newurl.indexOf("./")) == 0){
	    if (a == 0) { //  remove ./
	      newurl = newurl.substring(3);
	      base = base.substring(0, base.lastIndexOf("/"));
	    } else { // remove ../
	      newurl = newurl.substring(2);
	    }
	  }
	  newurl = base + "/" + newurl; 
	}
      }
      return newurl;
    }

    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
      Configuration conf = context.getConfiguration();

      String url = key.toString();      
      for (Text textVal : values) {
	if (textVal.toString().equals("1")) { // page already crawl
	  output.write("url", key, textVal);
	  context.getCounter(RegExpCrawler.counter.TOTAL_URL_COUNT).increment(1);
	  return ;
	}
      }
      long maxData = Long.parseLong(conf.get("maxdata2"));
      if (maxData != -1 && context.getCounter(RegExpCrawler.counter.DATA_COUNT).getValue() >= maxData) {
	  output.write("url", key, zero); // dont lost url if not truly a maxData
	  return;
      }
      output.write("url", key, new Text("1"));
      context.getCounter(RegExpCrawler.counter.URL_COUNT).increment(1);

      String[] regExpGroup = conf.getStrings("regExpGroup");
      String[] regExps = conf.getStrings("regExp");
      Boolean allowSubDomain = conf.getBoolean("allowSubDomain", false);
      Boolean allowDomainExit = conf.getBoolean("allowDomainExit", false);
      String dataSeparator =  conf.get("mapred.textoutputformat.separator");
      String baseDNS = conf.get("baseDNS");
      String connectInfo = conf.get("connectInfo");
      HttpURLConnection request;
      Map<String, Integer> crawlUrls = new HashMap<String, Integer>();

      HttpURLConnection.setFollowRedirects(true); // accept code 3XX redirection
      try {
	request = (HttpURLConnection)new URL(url).openConnection();
	request.setRequestMethod("GET");
	request.setConnectTimeout(4000); // 4s timeout
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	int responseCode = request.getResponseCode();
	if (responseCode != 200) {
          System.err.println("Receive strange HTTP response code (" + responseCode + ")");
	  System.err.println("\t"+url);
 	  context.getCounter(RegExpCrawler.counter.WARNING_COUNT).increment(1);
        }
	
	if (request.getHeaderField("Content-Length") != null && Long.parseLong(request.getHeaderField("Content-Length")) > 1200000) { // max 1.2Mo page per request
	    System.err.println("Page request aborted due to huge content length ("+(Long.parseLong(request.getHeaderField("Content-Length"))/1000)+"ko):");
	    System.err.println("\t"+url);
  	    context.getCounter(RegExpCrawler.counter.WARNING_COUNT).increment(1);
	    reader.close();
	    return ;
	}
        String html = "";
	int readline = 0;
	for (String line; (line = reader.readLine()) != null;) {
	  html += line + "\n";
	  if ((readline == 20 || readline == 50) && testCannonicUrl(html, url)) {
	    System.err.println("Page request aborted due to cannonic shild.");
	    System.err.println("\t"+url);
  	    context.getCounter(RegExpCrawler.counter.WARNING_COUNT).increment(1);
	    reader.close();
	    return ;
	  }
	  readline++;
	}
	// search all url
        Matcher urlMatch = Pattern.compile("href[ ]*=[ ]*('|\")([^\"\']+)('|\")+/?", Pattern.CASE_INSENSITIVE).matcher(html);
	while (urlMatch.find()) {
	  String newurl = parseUrl(connectInfo, url, urlMatch.group(2));
	  String domain = getUrlDomain(newurl);
	  if (url != newurl && !testCannonicUrl(html, url) &&
	      newurl.indexOf('\n') == -1 &&
	      newurl.indexOf('\r') == -1 &&
	      (allowDomainExit || (allowSubDomain && domain.lastIndexOf(baseDNS) == (domain.length() - baseDNS.length())) || domain.equals(baseDNS))) {
	    crawlUrls.put(newurl, 0);
	  }
	}
	Set<String> uniqueNewUrls = crawlUrls.keySet();
	for (String newurl : uniqueNewUrls) {
	  output.write("url", new Text(newurl), zero); // saving crawl urls
	  context.getCounter(RegExpCrawler.counter.FOUND_URL_COUNT).increment(1);
	}
	// search all elements by regexp
	Matcher regExpMatcher[] = new Matcher[regExps.length];
	int found = regExps.length;
	int end = 0;
	while (found == regExps.length) {
	    found = 0;
	    String data = "";
	    for (int i = 0; i < regExps.length; ++i) { // setup field regexp
	      regExpMatcher[i] = Pattern.compile(regExps[i]).matcher(html);
	    }
	    for (int i = 0; i < regExpMatcher.length;) {
		if (regExpMatcher[i].find()) {
		  found++;
		  if (regExpGroup[i].length() == 0) { // all regexp
		    data += regExpMatcher[i].group();
		  } else {
		    try { // select regexp group by id
		      data += regExpMatcher[i].group(Integer.parseInt(regExpGroup[i]));
		    } catch (NumberFormatException nfe) {  // select regexp group by name
		      data += regExpMatcher[i].group(regExpGroup[i]);
		    }
		  }
		  end = regExpMatcher[i].end();
		}
		data += (++i < regExpMatcher.length ? dataSeparator : "");
	    }
	    html = html.substring(end); // exclude last rows
	    if (found == regExps.length) {
	      CRC32 id = new CRC32();
	      id.update(data.getBytes(), 0, data.getBytes().length);
	      output.write("data", new Text(Long.toString(id.getValue())), new Text(data)); // saving crawl data
	      context.getCounter(RegExpCrawler.counter.DATA_COUNT).increment(1);
	    }
	}
	reader.close();
      } catch (Exception e) {
	System.err.println("Crawl error: "+e.toString());
	System.err.println("\t"+url);
	context.getCounter(RegExpCrawler.counter.ERROR_COUNT).increment(1);
      }
    }

    public static String getUrlDomain(String url) {
      int index;
      String domain = url.substring(url.indexOf("://") + 3);
      if ((index = domain.indexOf('@')) != -1){
        domain = domain.substring(index + 1);
      }
      if ((index = domain.indexOf('/')) != -1){
        domain = domain.substring(0, index);
      }
      if ((index = domain.indexOf(':')) != -1){
        domain = domain.substring(0, index);
      }
      return domain;
    }
  }

  public static void main(String[] args) throws Exception {
    if (args.length < 5) {
      System.out.println("usage: RegExpCrawler " +
			 "-url \"https://en.wikipedia.org/wiki/Strassen_algorithm\" " +
			 "-regexp 1 \"<h1[^>]+id=\\\\\\\"firstHeading\\\\\"[^>]*>([^<]+)<?\" " +
			 "-regexp 1 \'<p>(.*(?!<\\\\/p>))<?\' " +
			 "-report crawlReport.txt " +
			 "-o crawlData.csv " +
			 "-maxdata 40 " +
			 "-allowsubdomain " +
			 "-allowdomainexit"
			 );
      return ;
    }
    
    // default value
    String url = "";
    ArrayList<String> regExp = new ArrayList<String>();
    ArrayList<String> regExpGroup = new ArrayList<String>();
        
    int maxData = -1; // infinit crawl
    String outputPath = "crawlerResult.csv";
    String outputReport = "crawlReport.txt";
    Boolean allowSubDomain = false;
    Boolean allowDomainExit = false;
    
    // arguments parser
    for (int i = 0; i < args.length; ++i) {
	switch (args[i]) {
	  case "-allowsubdomain":
	    allowSubDomain = true;
	    break;
	  case "-allowdomainexit":
	    allowDomainExit = true;
	    break;
	  case "-report":
	    if (i + 1 >= args.length)
	      break;
	    outputReport = args[++i];
	    break;
	  case "-o":
	    if (i + 1 >= args.length)
	      break;
	    outputPath = args[++i];
	    break;
	  case "-maxdata":
	    if (i + 1 >= args.length)
	      break;
	    maxData = Integer.parseInt(args[++i]);
	    break;
	  case "-url":
	    if (i + 1 >= args.length)
	      break;
	    url = args[++i];
	    break;
	  case "-regexp":
	    if (i + 2 >= args.length)
	      break;
	    regExpGroup.add(args[++i]);
	    regExp.add(args[++i]);
	    System.out.println(args[i]);
	    break;
	  default:
	    System.err.println("Unknown parameter: " + args[i]);
	}
    }

    if (url.length() == 0) {
      System.out.println("Please specify an URL.");
      return ;
    }
    if (regExp.size() < 1) {
      System.out.println("Please specify a regular expression.");
      return ;
    }

    String connectInfo = url.substring(0, url.substring(9).indexOf("/") + 9);
    String domain = RegExpCrawler.HttpReducer.getUrlDomain(url);

    Path tmpIO = new Path("./tmpRegExpCrawler");
    Path tmpIO2 = new Path("./tmpRegExpCrawlerSwap");
    Path tmpDataIO = new Path("./tmpRegExpCrawlerData");
    Path tmpDataIO2 = new Path("./tmpRegExpCrawlerDataSwap");
    Path rep = new Path(outputReport);
    Configuration conf = new Configuration();
    
    conf.setBoolean("allowSubDomain", false);
    conf.setBoolean("allowDomainExit", false);
    conf.set("maxdata", Integer.toString(maxData));
    conf.set("baseDNS", domain);
    conf.set("connectInfo", connectInfo);
    conf.setStrings("regExpGroup", regExpGroup.toArray(new String[regExpGroup.size()]));
    conf.setStrings("regExp",  regExp.toArray(new String[regExp.size()]));
    conf.set("mapred.textoutputformat.separator", ";");

    // tmp file setup
 
    rep.getFileSystem(conf).delete(rep, true);
    tmpIO.getFileSystem(conf).delete(tmpIO, true);
    tmpIO.getFileSystem(conf).delete(tmpIO, true);
    tmpIO2.getFileSystem(conf).delete(tmpIO2, true);
    tmpDataIO.getFileSystem(conf).delete(tmpDataIO, true);
    tmpDataIO2.getFileSystem(conf).delete(tmpDataIO2, true);
    File tmpDir = new File(tmpIO.toString());
    if (!tmpDir.exists()) {
      try {
        tmpDir.mkdir();
      } 
      catch(SecurityException se){
      }   
    }
    Writer tmpFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpIO.toString() + "/url-r-00000"), "utf-8"));
    tmpFile.write(url + ";0");
    tmpFile.close();
    
    long startTime = Instant.now().toEpochMilli();
    long crawlData = 0;
    
    for (int depth = 0; maxData == -1 || crawlData < maxData; ++depth) {
      conf.set("maxdata2", Integer.toString((int)(maxData * 1.5) - (int)crawlData));
      Job job = Job.getInstance(conf, "RegExp page crawler"); // setup main hadoop crawl task
      job.setJarByClass(RegExpCrawler.class);
      job.setMapperClass(RegExpCrawler.TokenizerMapper.class);
      job.setReducerClass(RegExpCrawler.HttpReducer.class);
      job.setOutputKeyClass(Text.class);
      job.setOutputValueClass(Text.class);
      FileInputFormat.setInputPaths(job, tmpIO + "/url-r-00000");
      FileOutputFormat.setOutputPath(job, tmpIO2);
      MultipleOutputs.addNamedOutput(job, "data", TextOutputFormat.class, Text.class, Text.class);
      MultipleOutputs.addNamedOutput(job, "url", TextOutputFormat.class, Text.class, Text.class);
      if (!job.waitForCompletion(true)) {
        System.err.println("Failing crawl");
        return ;
      }
      
      // removing duplicated data
      Job job2 = Job.getInstance(conf, "Duplicated data removal");
      job2.setJarByClass(RegExpCrawler.class);
      job2.setMapperClass(RegExpCrawler.TokenizerMapper.class);
      job2.setReducerClass(RegExpCrawler.DistinctDataReducer.class);
      job2.setOutputKeyClass(Text.class);
      job2.setOutputValueClass(Text.class);
      FileInputFormat.addInputPaths(job2, tmpIO2 + "/data-r-00000"); // new data
      if (depth > 0) {
	  FileInputFormat.addInputPaths(job2, tmpDataIO.toString()); // previous data
      }
      FileOutputFormat.setOutputPath(job2, tmpDataIO2); // append/distinct data output
      if (!job2.waitForCompletion(true)) {
          System.err.println("Failing duplicated data removal");
          return ;
      }
      
      // report state
      BufferedReader lastDataFile = new BufferedReader(new InputStreamReader(new FileInputStream(tmpDataIO2.toString() + "/part-r-00000")));
      BufferedWriter reportFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputReport, true)));
      String report = "";
      crawlData = job2.getCounters().findCounter(RegExpCrawler.counter.DATA_COUNT).getValue();
      report += " DATA COUNT:" + crawlData + " (" + (crawlData / ((float)maxData / 100)) +"%)\n";
      report += " DEPTH PASS: " + (depth + 1) + "\n";
      report += " TOTAL CRAWL URL:" + job.getCounters().findCounter(RegExpCrawler.counter.TOTAL_URL_COUNT).getValue() + "\n";
      report += " CRAWL URL:" + job.getCounters().findCounter(RegExpCrawler.counter.URL_COUNT).getValue() + "\n";
      report += " FOUND URL:" + job.getCounters().findCounter(RegExpCrawler.counter.FOUND_URL_COUNT).getValue() + "\n";
      report += " ERROR COUNT:" + job.getCounters().findCounter(RegExpCrawler.counter.ERROR_COUNT).getValue() + "\n";
      report += " WARNING COUNT:" + job.getCounters().findCounter(RegExpCrawler.counter.WARNING_COUNT).getValue() + "\n";
      if (crawlData < maxData && crawlData != 0) {
	  long remainingSec = ((Instant.now().toEpochMilli() - startTime) / crawlData) * (maxData - crawlData) / 1000;
	  report += (" REMAINING TIME: " + remainingSec / 60 + "m " + remainingSec % 60 + "s\n");
      }
      report += " LAST FOUND DATA:\n";
      String dataLine;
      for (int i = 0; (dataLine = lastDataFile.readLine()) != null && i < 10; ++i){
	  report += "\t" + dataLine + "\n";
      }
      lastDataFile.close();
      report += "\n\n";
      System.out.print(report);
      reportFile.write(report.toCharArray(), 0, report.toCharArray().length);
      reportFile.close();

      // IO swaping
      String swapIO = tmpIO.toString();
      tmpIO = tmpIO2;
      tmpIO2 = new Path(swapIO);
      tmpIO2.getFileSystem(conf).delete(tmpIO2, true);
      swapIO = tmpDataIO.toString();
      tmpDataIO = tmpDataIO2;
      tmpDataIO2 = new Path(swapIO);
      tmpDataIO2.getFileSystem(conf).delete(tmpDataIO2, true);
    }

    // moving output data to final destination
    Files.move(Paths.get(tmpDataIO.toString() + "/part-r-00000"), Paths.get(outputPath), StandardCopyOption.REPLACE_EXISTING);

    // removing tmpIO
    tmpIO.getFileSystem(conf).delete(tmpIO, true);
    tmpIO2.getFileSystem(conf).delete(tmpIO2, true);
    tmpDataIO.getFileSystem(conf).delete(tmpDataIO, true);
    tmpDataIO2.getFileSystem(conf).delete(tmpDataIO2, true);
  }
}
